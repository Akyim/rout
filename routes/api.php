<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/wapi',function(Request $request){
	return $request->ip();
});

/* Validation Url*/
Route::post('/yxpiJTxMMCaV7COn14fv', function () {
    
    return response()->json([
        'ResultCode' => 0,
        'ResultDesc' => 'Payment Validation Success',
        'ThirdPartyTransID' => 'PB0001',
        'RequestBy' => $request->ip()
    ]);
});

/* COnfirmation Url */
Route::post('/P7hVAAMzzD8bc0Tw07nI', function(Request $request){

	$requestData = $request->getContent();
	
    return response()->json([
        'ResultCode' => 0,
        'ResultDesc' => 'Confirmation Service request accepted successfully',
        'RequestBy' => $request->ip()
    ]);

});